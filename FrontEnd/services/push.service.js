class PushService {
	constructor($state) {
		'ngInject';
		this.deviceId = "";
		this.$state = $state;

	}
	registration(){
		const push = PushNotification.init({
			android: {

			}
		});

		push.on('registration', (data) => {
			console.log(data);
			console.log(data.registrationId);
			this.deviceId = data.registrationId;
			// document.location = `${location.origin +
			// location.pathname}#/todo?answerId=1574752682582`;
		});

		push.on('notification', (data) => {
			// this.$state.go('description', {descriptionId:data.id})
			console.log(data);
			this.$state.go('description', {id: data.additionalData.id})
		});

		push.on('error', (e) => {
			console.log(e.message);
		});
	}


}

angular.module("push.service", []).service("PushService", PushService);
