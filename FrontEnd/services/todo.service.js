const TODO_API = "http://10.10.2.2:3012/";
class TodoService {
  constructor($http, PushService) {
      'ngInject';
     this.$http = $http;
     this.pushService = PushService;
     this.todos = [];
     this.todoItem = {};

  }

  getData() {
        return this.$http({
            method: 'GET',
            url: TODO_API
        }).then((response) => {
            this.todos = response.data;
        });
    }

    getById(id) {
    return this.$http({
      method: "GET",
      url: `${TODO_API}${id}`
    }).then(function(){return this.todoItem = this.todos.find((element) => element.id === id )});
  };

  add(task)  {
    this.todos.push({
      id: Date.now(),
      done: false,
      task: task,
      description: "some"
    });
     this.index = Number(this.todos.length) - 1;
     this.data = this.todos[this.index];
     this.posted = JSON.stringify(this.data);
     this.$http.post(`${TODO_API}`, this.posted)
  }

  delete(id)  {
    this.$http.delete(`${TODO_API}${id}`);
    this.todos = this.todos.filter(element => element.id !== id );

  }

  edit(id, description, time, task){
      this.todoItem.time = time;
      let hour = new Date(this.todoItem.time).getHours();
      let hourNow = new Date().getHours();
      let hourResult = (hour - hourNow) * 3600 * 1000;

      let minutes = new Date(this.todoItem.time).getMinutes();
      let minutesNow = new Date().getMinutes();
      let minutesResult = (minutes - minutesNow) * 60 * 1000;

      let seconds = new Date(this.todoItem.time).getSeconds();
      let secondsNow = new Date().getSeconds();
      // let secondsResult = (seconds - secondsNow) * 1000;
      this.todoItem.id = id;
      this.todoItem.countdown = hourResult + minutesResult;
      this.todoItem.task = task;
      this.todoItem.description = description;
      this.todoItem.deviceId = this.pushService.deviceId;
      // console.log(this.todoItem);
      this.$http.patch(`${TODO_API}${id}`, this.todoItem)

  }

}

angular.module("todo.service", []).service("TodoService", TodoService);
