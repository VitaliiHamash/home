angular.module('todo.state', []).config(function($stateProvider, $urlRouterProvider) {
	'ngInject';
	$urlRouterProvider.otherwise("/home");
	$stateProvider

		.state("home", {
			url: "/home",

			component: "doList"
		})

		.state("description", {
			url: "/description?id",
			component: "editDescription"
		});
});
