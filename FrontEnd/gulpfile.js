const gulp = require('gulp');
const ngAnnotate = require('gulp-ng-annotate');
const concat = require('gulp-concat');

// const babel = require('babel');
// const uglify = require('gulp-uglify');

const scripts = require('./scripts');
const styles = require('./styles');

gulp.task('css', function () {
  return gulp.src(styles)
		.pipe(concat('main.css'))
	    .pipe(ngAnnotate({add: true}))
		.pipe(gulp.dest('dist/css'))
});

gulp.task('js', function () {
  return gulp.src(scripts)
	  .pipe(ngAnnotate({add: true}))
	  .pipe(concat('scripts.js', {newLine: ";"}))
		.pipe(gulp.dest('./dist/js'))
});

gulp.task('default', gulp.series(['css', 'js']));
