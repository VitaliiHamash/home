const doListComponent = {
  templateUrl: "components/do-list/do-list.component.html",
  controller: class {
    constructor(TodoService, $stateParams) {
      'ngInject';
      this.todoService = TodoService;
      this.$stateParams  = $stateParams;
      this.id = $stateParams.id;

      // this.todos = TodoService.todos;

    }
    $onInit(){
      this.todoService.getData();
    }

  }
};

angular
  .module("todo.dolist.component", [])
  .component("doList", doListComponent);
