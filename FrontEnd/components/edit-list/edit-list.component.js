const editListComponent = {
  templateUrl: "components/edit-list/edit-list.component.html",
  controller: class {
    constructor(TodoService, $stateParams) {
      'ngInject';
      this.$stateParams = $stateParams;
      this.todoService = TodoService;


    }

    $onInit()  {

      const id = +this.$stateParams.id;
      this.todoService.getById(id);
      this.todoItem =this.todoService.todos.find(element => element.id === +id );
    }
  }
};

angular
  .module("todo.edit.component", [])
  .component("editDescription", editListComponent);
