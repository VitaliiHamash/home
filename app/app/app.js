const express = require('express');
const bodyParser = require('body-parser');
const file = require('file-system');
const corsMiddleware = require("cors");
const app = express();

let FCM = require('fcm-node');

let serverKey = 'AAAAQkeypHc:APA91bFD7da1EbOHrZxn9s76tHY4u2IqsNhWoImWXV_MrtjdSJ913Bzj8XWVfcCZ9-OAlTGPGL0YYi8mFY5kRVy3HJkdDTaOcfDu4sQEm-3ojAarf9HJh3b-7yaFXF56O-lt57xBjk-8';
let fcm = new FCM(serverKey);





app.use(
    corsMiddleware({
        domain: "http://10.10.2.2:3012"
    })
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));

//app.get('/'... і app.get('/home'... це різні посилання
//після додавання нових роутів або після редагування існуючих потрібно перезавантажити сервер

app.get('/', function (req, res) {
    file.readFile('storage.json', 'utf8', function (err, data) {
        if (err) throw err; // we'll not consider error handling for now
        let todos = JSON.parse(data);
        res.send(todos);
    });
});


// динамічний параметр
app.get('/:id?', function (req, res) {


    let readStorage = file.readFileSync("storage.json");
    let todos = JSON.parse(readStorage);
    console.log(req.params.id);

    let todoItem = todos.find(function (x) {
        return +x.id ===  +req.params.id;

    });
    res.send(todoItem)

});

function pushNotification(object){
    let message = {
        to: object.deviceId,
        data: {
            title: object.task,
            body: object.description,
            id: object.id
        }

    };


    setTimeout(function () {
        fcm.send(message, function(err, response){
            if (err) {
                console.log("Something has gone wrong!")
            } else {
                console.log("Successfully sent with response: ", response)
            }
        })
    }, Number(object.countdown))
}

app.post('/', function (req, res) {
    file.readFile('storage.json', 'utf8', function (err, data) {
        if (err) throw err; // we'll not consider error handling for now
        let todos = JSON.parse(data);
        console.log(todos)
        let todoItem = {
            id: req.body.id,
            done: false,
            task: req.body.task,
            description:"some"
        }

        todos.push(todoItem);
        console.log(todoItem)
        let posted = JSON.stringify(todos);
        file.writeFile("storage.json", posted);

    });

})

app.delete('/:id', function (req,res) {
    //обробка даних з сховища
    file.readFile('storage.json', 'utf8', function (err, data) {
        if (err) throw err; // we'll not consider error handling for now
        let todos = JSON.parse(data);
        let id = +req.params.id;
        let filtered = todos.filter(element => element.id !== id );
        let write = JSON.stringify(filtered);
        file.writeFile("storage.json", write);
        res.send(filtered);
    })
});

app.patch('/:id', function (req, res) {
    let readStorage = file.readFileSync("storage.json");
    let todos = JSON.parse(readStorage);
    let patched = todos.find(element => element.id === +req.params.id );
    patched.description = req.body.description;
    patched.countdown  = req.body.countdown;
    patched.task = req.body.task;
    patched.deviceId= req.body.deviceId;
    patched.id = req.body.id;
    console.log(patched);
    let data = JSON.stringify(todos, null, 2);
    file.writeFile("storage.json", data);
    pushNotification(patched);
});



app.listen(3012, function () {
    console.log('API app started')
});



function ignoreFavicon(req, res, next) {
    if (req.originalUrl === '/favicon.ico') {
        res.status(204).json({nope: true});
    } else {
        next();
    }
}
app.use(ignoreFavicon);


